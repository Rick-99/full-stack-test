<?php

namespace App\Http\Controllers;

use App\Services\MovieService;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    private $movieService;

    public function __construct(MovieService $movieService)
    {
        $this->movieService = $movieService;
    }

    public function index(Request $request)
    {
        $movies = $this->movieService->getAllMovies();
        return response()->json($movies);
    }

    public function store(Request $request)
    {
        $movieData = $request->validate([
            'title' => 'required|string|max:20',
            'director' => 'required|string',
            'category' => 'required|string|max:20',
            'genres' => 'required|string|max:20',
            'release_year' => 'required|integer|max:20',
            'duration' => 'required|integer',
            'synopsis' => 'required|string|max:100',
        ]);

        $movie = $this->movieService->createMovie($movieData);
        return response()->json($movie, 201);
    }

    public function show($movieId)
    {
        $movie = $this->movieService->getMovieById($movieId);

        if (!$movie) {
            return response()->json(['message' => 'Filme não encontrado'], 404);
        }

        return response()->json($movie);
    }

    public function update(Request $request, $movieId)
    {
        $movieData = $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'genre' => 'required|string|max:100',
            'director' => 'required|string|max:255',
            'release_year' => 'required|integer'
        ]);

        $movie = $this->movieService->updateMovie($movieId, $movieData);

        if (!$movie) {
            return response()->json(['message' => 'Filme não encontrado'], 404);
        }

        return response()->json($movie);
    }

    public function destroy($movieId)
    {
        $movie = $this->movieService->deleteMovie($movieId);

        if (!$movie) {
            return response()->json(['message' => 'Filme não encontrado'], 404);
        }

        return response()->json(['message' => 'Filme excluído com sucesso']);
    }
}