<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public  $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(Request $request)
    {
        return $this->userService->getAllUsers();
    }

    public function store(StoreUserRequest $request)
    {
        return $this->userService->createUser($request);
    }

    public function show($userId)
    {
        return $this->userService->getUserById($userId);
    }

    public function update(UpdateUserRequest $request, $userId)
    {
        return $this->userService->updateUser($userId, $request->all());
    }

    public function destroy($userId)
    {
        return $this->userService->deleteUser($userId);
    }
}
