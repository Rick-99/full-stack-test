<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserService
{
    public function getAllUsers()
    {
        $allUsers = User::all();
        return response()->json(['success' => true, 'data' => $allUsers, 'message' => 'Users found'], 201);
    }

    public function getUserById($userId)
    {
        $user = User::find($userId);

        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }
        return response()->json(['success' => true, 'data' => $user, 'message' => "User {$user->name} found"], 201);

    }

    /**
     * Função para registro de usuário
     */
    public function createUser($userData)
    {
        // Criptografia de senha
        $userData['password'] = Hash::make($userData['password']);

        // Criação do usuário no banco de dados
        $user = User::create($userData->all());

        // Geração de token de autenticação
        if ($token = $user->createToken('auth-token')) {
            $response = [
                'success' => true,
                'data' => $user,
                'message' => 'Usuário registrado com sucesso!',
                'token' => $token->plainTextToken,
            ];
        } else {
            $response = [
                'success' => true,
                'data' => $user,
                'message' => 'Usuário registrado com sucesso!',
            ];
        }

        return response()->json($response, 201);
        // return response()->json(['success' => true, 'data' => $user, 'message' => 'User created'], 201);
    }


    public function updateUser($userId, $userData)
    {
        if (!$userData) {
            return response()->json(['message' => 'Usuário não encontrado'], 404);
        }

        $user = User::find($userId);

        if (!$user) {
            return null;
        }

        $user->update($userData);
        return response()->json($user);
    }

    public function deleteUser($userId)
    {
        $user = User::find($userId);

        if (!$user) {
            return response()->json(['success' => false, 'message' => "User {$user->name} found"], 404);
        }

        $user->delete();
        return response()->json(['success' => true, 'message' => 'User deleted successfully']);
    }
}
