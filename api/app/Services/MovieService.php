<?php

namespace App\Services;

use App\Models\Movie;

class MovieService
{
    public function getAllMovies()
    {
        return Movie::all();
    }

    public function getMovieById($movieId)
    {
        return Movie::find($movieId);
    }

    public function createMovie($movieData)
    {
        return Movie::create($movieData);
    }

    public function updateMovie($movieId, $movieData)
    {
        $movie = Movie::find($movieId);

        if (!$movie) {
            return null;
        }

        $movie->update($movieData);
        return $movie;
    }

    public function deleteMovie($movieId)
    {
        $movie = Movie::find($movieId);

        if (!$movie) {
            return null;
        }

        $movie->delete();
        return $movie;
    }
}