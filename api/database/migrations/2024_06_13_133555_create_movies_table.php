<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('public.movies', function (Blueprint $table) {
            $table->id();
            $table->string("title");
            $table->string("director");
            $table->string("category");
            $table->string("genres");
            $table->timestamp("release_year");
            $table->integer("duration");
            $table->string("synopsis");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists("public.movies");
    }
};
