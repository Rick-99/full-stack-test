# Titulo 
full-stack-test

# Descrição
Este projeto é uma aplicação web que utiliza Laravel no backend NextJs no frontend e PostgreSQL como banco de dados. O backend está containerizado em Docker para facilitar o desenvolvimento e a implantação. O Laravel é um framework PHP robusto e popular para desenvolvimento web, enquanto o NextJs é uma biblioteca JavaScript para criar interfaces de usuário interativas e dinâmicas.

# Pré-requisitos
Para executar este projeto, você precisará ter os seguintes softwares instalados em seu computador:

-Docker: Ferramenta de containerização (opcional, mas recomendado)
-PHP: Versão 8.2 ou superior
-Composer: Gerenciador de dependências PHP
-Node.js: Ambiente de execução JavaScript (opcional, para alguns componentes frontend)
-npm: Gerenciador de pacotes JavaScript (opcional, para alguns componentes frontend)

# Passo a passo para rodar o projeto
Clone Repositório
https://gitlab.com/Rick-99/full-stack-test.git

# Atualize as variáveis de ambiente do arquivo .env

APP_NAME=Laravel
APP_URL=http://localhost

DB_CONNECTION=pgsql
DB_HOST=localhost
DB_PORT=5440
DB_DATABASE=nome_que_desejar_db
DB_USERNAME=nome_usuario
DB_PASSWORD=senha_aqui

# Suba os containers do projeto
docker-compose up -d

# Instalar as dependências do projeto
composer install

# Caso Docker não baixe corretamente
Para Laravel
composer intall

# Instalar as dependências do front-end
npm install
npm install next-auth